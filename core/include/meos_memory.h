/*
 *  Filename:      meos_memory.h
 *  Author:         Erick Huang<erickhuang1989@gmail.com>
 *  Revised:        2014-12-16
 *  Description:   This module defines the MEOS memory control functions. 
 *
 *  Copyright (c) Erick Huang. All rights reserved.
 */

#ifndef MEOS_MEMORY_H
#define MEOS_MEMORY_H

#ifdef __cplusplus
extern "C" {
#endif


extern void meos_mem_init( void );
extern void *meos_mem_alloc( uint16 size );
extern void meos_mem_free( void *ptr );


#ifdef __cplusplus
}
#endif

#endif
