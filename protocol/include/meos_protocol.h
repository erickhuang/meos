/*
 *  Filename:      meos_protocol.h
 *  Author:         Erick Huang<erickhuang1989@gmail.com>
 *  Revised:        2014-12-23
 *  Description:   
 *
 *  Copyright (c) Erick Huang. All rights reserved.
 */

#ifndef MEOS_PROTOCOL_H
#define MEOS_PROTOCOL_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
  	meos_msg_data_hdr_t  hdr;          
  	uint8 opcode;
} ProtEventHdr_t;

typedef struct
{
  	meos_msg_data_hdr_t  hdr;          
  	uint8 opcode;
	
	uint8 data;
} ProtEventInitDone_t;

typedef union
{
  	ProtEventHdr_t       prot;
  	ProtEventInitDone_t  initDone;
	/* Add new Event Data Struct to here */
} ProtEvent_t;

typedef void (*ProtEventCB_t)( ProtEvent_t *pEvent );
typedef void (*ProtOthersCB_t)(void);

typedef struct
{
  	ProtEventCB_t   eventCB;   // Event callback.
  	ProtOthersCB_t  otherCB;   // Others callback.
} ProtCBs_t;

/* Pass to App Events */
#define PROT_EVENT_INIT_DONE 0x00


/* Config Parameters */
#define PROT_PARAM_1         0x01
#define PROT_PARAM_2         0x02



extern void   Prot_Init( uint8 task_id );
extern uint16 Prot_ProcessEvent( uint8 task_id, uint16 events );


extern Status_t Prot_Start( ProtCBs_t *appCallbacks );
extern Status_t Prot_RegisterForMsg( uint8 taskId );
extern Status_t Prot_RegisterAppCBs( ProtCBs_t *appCallbacks );

extern Status_t Prot_SetParameter( uint8 param, uint8 len, void *pValue );
extern Status_t Prot_GetParameter( uint8 param, uint8 len, void *pValue );


#ifdef __cplusplus
}
#endif

#endif
