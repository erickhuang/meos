/*
 *  Filename:      hal_key.h
 *  Author:         Erick Huang<erickhuang1989@gmail.com>
 *  Revised:        2014-12-16
 *  Description:   
 *
 *  Copyright (c) Erick Huang. All rights reserved.
 */

#ifndef HAL_KEY_H
#define HAL_KEY_H

#ifdef __cplusplus
extern "C" {
#endif


/* Interrupt option - Enable or disable */
#define HAL_KEY_INTERRUPT_DISABLE    0x00
#define HAL_KEY_INTERRUPT_ENABLE     0x01

/* Key state - shift or nornal */
#define HAL_KEY_STATE_NORMAL          0x00
#define HAL_KEY_STATE_SHIFT           0x01



extern void HalKeyInit( void );
extern void HalKeyConfig(bool_t interruptEnable, halKeyCBack_t cback);
extern uint8 HalKeyRead( void );
extern void HalKeyPoll( void );



#ifdef __cplusplus
}
#endif

#endif
