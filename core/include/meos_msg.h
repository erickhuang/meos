/*
 *  Filename:      meos_msg.h
 *  Author:         Erick Huang<erickhuang1989@gmail.com>
 *  Revised:        2014-12-16
 *  Description:   
 *
 *  Copyright (c) Erick Huang. All rights reserved.
 */

#ifndef MEOS_MSG_H
#define MEOS_MSG_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
  	uint8  type;
  	uint8  status;
} meos_msg_data_hdr_t;



extern void meos_msg_init( void );
extern uint8 *meos_msg_allocate( uint16 len );
extern Status_t meos_msg_deallocate( uint8 *msg_ptr );
extern Status_t meos_msg_send( uint8 dest_task, uint8 *msg_ptr, bool_t urgent );
extern uint8 *meos_msg_receive( uint8 task_id );






#ifdef __cplusplus
}
#endif

#endif
