/*
 *  Filename:      meos_pwr_mgr.h
 *  Author:         Erick Huang<erickhuang1989@gmail.com>
 *  Revised:        2014-12-16
 *  Description:   This file contains the Power Management API.
 *
 *  Copyright (c) Erick Huang. All rights reserved.
 */

#ifndef MEOS_PWR_MGR_H
#define MEOS_PWR_MGR_H

#ifdef __cplusplus
extern "C" {
#endif


/* With PWRMGR_ALWAYS_ON selection, there is no power savings and the
 * device is most likely on mains power. The PWRMGR_BATTERY selection allows
 * the Processor to enter SLEEP LITE state or SLEEP DEEP state.
 */
#define PWRMGR_ALWAYS_ON  0
#define PWRMGR_BATTERY    1

/* The PWRMGR_CONSERVE selection turns power savings on, all tasks have to
 * agree. The PWRMGR_HOLD selection turns power savings off.
 */
#define PWRMGR_CONSERVE 0
#define PWRMGR_HOLD     1



extern void meos_pwrmgr_init( void );
extern void meos_pwrmgr_device( uint8 pwrmgr_device );
extern Status_t meos_pwrmgr_task_state( uint8 task_id, uint8 state );
extern void meos_pwrmgr_standby( void );






#ifdef __cplusplus
}
#endif

#endif
