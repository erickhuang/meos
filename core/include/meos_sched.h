/*
 *  Filename:      meos_sched.h
 *  Author:         Erick Huang<erickhuang1989@gmail.com>
 *  Revised:        2014-12-16
 *  Description:   
 *
 *  Copyright (c) Erick Huang. All rights reserved.
 */

#ifndef MEOS_SCHED_H
#define MEOS_SCHED_H

#ifdef __cplusplus
extern "C" {
#endif

extern Status_t meos_init_system( void );
extern void  meos_start_system( void );
extern void  meos_run_system( void );





#ifdef __cplusplus
}
#endif

#endif
