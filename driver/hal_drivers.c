/*
 *  Filename:      hal_drivers.c
 *  Author:         Erick Huang<erickhuang1989@gmail.com>
 *  Revised:        2014-12-16
 *  Description:   This file contains the interface to the Drivers Service.
 *
 *  Copyright (c) Erick Huang. All rights reserved.
 */
#include "meos_api.h"
#include "hal_drivers.h"
#include "hal_uart.h"
#include "hal_key.h"
#include "hal_led.h"

/* Task ID for internal task/event processing */
uint8 Hal_TaskID;


void Hal_Init( uint8 task_id )
{
	Hal_TaskID = task_id;
}

uint16 Hal_ProcessEvent( uint8 task_id, uint16 events )
{
	VOID task_id;

  	if ( events & SYS_EVENT_MSG )
  	{
  		uint8 *msgPtr;
	
    	msgPtr = meos_msg_receive(Hal_TaskID);
    	while (msgPtr)
    	{
      		/* Do something here - for now, just deallocate the msg and move on */

      		/* De-allocate */
      		meos_msg_deallocate( msgPtr );
			
      		/* Next  Message */
      		msgPtr = meos_msg_receive( Hal_TaskID );
    	}
		
    	return events ^ SYS_EVENT_MSG;
  	}

#if (defined (BLINK_LEDS)) && (HAL_LED == TRUE)
  	if ( events & HAL_LED_BLINK_EVENT )
  	{
    	//HalLedUpdate();
    	return events ^ HAL_LED_BLINK_EVENT;
  	}
#endif

#if (defined HAL_KEY) && (HAL_KEY == TRUE)
  	if (events & HAL_KEY_EVENT)
  	{
    	/* Check for keys */
    	HalKeyPoll();
    	return events ^ HAL_KEY_EVENT;
  	}
#endif

  	return 0;
}


/**************************************************************************************************
 * @fn      Hal_ProcessPoll
 *
 * @brief   This routine will be called by OSAL to poll UART, TIMER...
 *
 * @param   task_id - Hal TaskId
 *
 * @return  None
 **************************************************************************************************/
void Hal_ProcessPoll( void )
{
  	/* UART Poll */

  	/* SPI Poll */
}

/**************************************************************************************************
 * @fn      Hal_DriverInit
 *
 * @brief   Initialize HW - These need to be initialized before anyone.
 *
 * @param   task_id - Hal TaskId
 *
 * @return  None
 **************************************************************************************************/
void Hal_DriverInit (void)
{
  	/* TIMER */
	#if (defined HAL_TIMER) && (HAL_TIMER == TRUE)
	#endif

  	/* LED */
	#if (defined HAL_LED) && (HAL_LED == TRUE)
  		//HalLedInit();
	#endif

  	/* UART */
	#if (defined HAL_UART) && (HAL_UART == TRUE)
  		//HalUARTInit();
	#endif

  	/* KEY */
	#if (defined HAL_KEY) && (HAL_KEY == TRUE)
  		HalKeyInit();
	#endif
}


