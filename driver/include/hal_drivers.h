/*
 *  Filename:      hal_drivers.h
 *  Author:         Erick Huang<erickhuang1989@gmail.com>
 *  Revised:        2014-12-16
 *  Description:   This file contains the interface to the Drivers service.
 *
 *  Copyright (c) Erick Huang. All rights reserved.
 */

#ifndef HAL_DRIVERS_H
#define HAL_DRIVERS_H

#ifdef __cplusplus
extern "C" {
#endif

#define HAL_LED_BLINK_EVENT                 0x0001
#define HAL_KEY_EVENT                       0x0002


extern uint8 Hal_TaskID;


extern void   Hal_Init( uint8 task_id );
extern uint16 Hal_ProcessEvent( uint8 task_id, uint16 events );

extern void Hal_DriverInit (void);
extern void Hal_ProcessPoll( void );


#ifdef __cplusplus
}
#endif

#endif
