/*
 *  Filename:      meos_api.h
 *  Author:         Erick Huang<erickhuang1989@gmail.com>
 *  Revised:        2014-12-16
 *  Description:   
 *
 *  Copyright (c) Erick Huang. All rights reserved.
 */

#ifndef MEOS_API_H
#define MEOS_API_H

#ifdef __cplusplus
extern "C" {
#endif

#include "meos_config.h"
#include "meos_types.h"
#include "meos_cpu.h"
#include "meos_comdef.h"
#include "meos_defs.h"
#include "meos_list.h"

#include "meos_pwr_mgr.h"
#include "meos_sched.h"
#include "meos_event.h"
#include "meos_msg.h"
#include "meos_timer.h"

#ifdef __cplusplus
}
#endif

#endif