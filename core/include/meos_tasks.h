/*
 *  Filename:      meos_tasks.h
 *  Author:         Erick Huang<erickhuang1989@gmail.com>
 *  Revised:        2014-12-16
 *  Description:   This file contains the Task definition and manipulation functions.
 *
 *  Copyright (c) Erick Huang. All rights reserved.
 */

#ifndef MEOS_TASKS_H
#define MEOS_TASKS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "meos_types.h"

#define TASK_NO_TASK      0xFF

/* Event handler function prototype */
typedef uint16 (*pTaskEventHandlerFn)( uint8 task_id, uint16 event );

extern const pTaskEventHandlerFn tasksArr[];
extern const uint8 tasksCnt;
extern uint16 tasksEvents[];

#ifdef __cplusplus
}
#endif

#endif