/*
 *  Filename:      hal_board.h
 *  Author:         Erick Huang<erickhuang1989@gmail.com>
 *  Revised:        2014-12-16
 *  Description:   
 *
 *  Copyright (c) Erick Huang. All rights reserved.
 */

#ifndef HAL_BOARD_H
#define HAL_BOARD_H

#ifdef __cplusplus
extern "C" {
#endif


typedef struct
{
  	meos_msg_data_hdr_t  hdr;
  	uint8   state; // shift
  	uint8   keys;  // keys
} keyChange_t;

typedef Status_t (*halKeyCBack_t)(uint8 keys, uint8 state);


// Initialization levels
#define OB_COLD     0
#define OB_WARM     1
#define OB_READY    2


extern void OnBoard_Init( uint8 level );

extern Status_t RegisterForKeys( uint8 task_id );
extern Status_t OnBoard_KeyCallback( uint8 keys, uint8 state );





#ifdef __cplusplus
}
#endif

#endif
