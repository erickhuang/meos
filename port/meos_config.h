/*
 *  Filename:      meos_config.h
 *  Author:         Erick Huang<erickhuang1989@gmail.com>
 *  Revised:        2014-12-16
 *  Description:   
 *
 *  Copyright (c) Erick Huang. All rights reserved.
 */

#ifndef MEOS_CONFIG_H
#define MEOS_CONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

#define xPOWER_SAVING

#define HAL_KEY    FALSE
#define HAL_LED    FALSE
#define HAL_UART    FALSE


#ifdef __cplusplus
}
#endif

#endif