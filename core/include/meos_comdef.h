/*
 *  Filename:      meos_comdef.h
 *  Author:         Erick Huang<erickhuang1989@gmail.com>
 *  Revised:        2014-12-16
 *  Description:   Type definitions and macros.
 *
 *  Copyright (c) Erick Huang. All rights reserved.
 */

#ifndef MEOS_COMDEFS_H
#define MEOS_COMDEFS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "meos_types.h"


/* Hint Keywords */
#define VOID (void)
#define NULL_OK
#define INP
#define OUTP

/* Generic Status Return Values */
#define SUCCESS                   0x00
#define FAILURE                   0x01
#define INVALID_PARAM             0x02
#define INVALID_TASK              0x03
#define MSG_BUFFER_NOT_AVAIL      0x04
#define INVALID_MSG_POINTER       0x05
#define INVALID_EVENT_ID          0x06


/* Generic Status return */
typedef uint8 Status_t;


/* Global System Events */
#define SYS_EVENT_MSG   0x8000  // A message is waiting event

/* Global Generic System Messages Type */
#define MSG_TYPE_KEY_CHANGE      0x01
#define MSG_TYPE_PROTOCOL_MSG_EVENT    0x02
#define MSG_TYPE_USER_APP_MSG    0x03

#ifdef __cplusplus
}
#endif

#endif