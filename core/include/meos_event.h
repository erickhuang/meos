/*
 *  Filename:      meos_event.h
 *  Author:         Erick Huang<erickhuang1989@gmail.com>
 *  Revised:        2014-12-16
 *  Description:   
 *
 *  Copyright (c) Erick Huang. All rights reserved.
 */

#ifndef MEOS_EVENT_H
#define MEOS_EVENT_H

#ifdef __cplusplus
extern "C" {
#endif




extern Status_t meos_set_event( uint8 task_id, uint16 event_flag );
extern Status_t meos_clear_event( uint8 task_id, uint16 event_flag );


#ifdef __cplusplus
}
#endif

#endif
