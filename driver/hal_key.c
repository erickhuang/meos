/*
 *  Filename:      hal_key.c
 *  Author:         Erick Huang<erickhuang1989@gmail.com>
 *  Revised:        2014-12-16
 *  Description:   
 *
 *  Copyright (c) Erick Huang. All rights reserved.
 */
#include "meos_api.h"
#include "hal_drivers.h"
#include "hal_board.h"
#include "hal_key.h"

static halKeyCBack_t pHalKeyProcessFunction;
static uint8 HalKeyConfigured;
static bool_t Hal_KeyIntEnable;   /* interrupt enable/disable flag */




void HalKeyInit( void )
{
	/* Configure GPIO */
	
  	/* Initialize callback function */
  	pHalKeyProcessFunction  = NULL;

  	/* Start with key is not configured */
  	HalKeyConfigured = FALSE;
}

void HalKeyConfig(bool_t interruptEnable, halKeyCBack_t cback)
{
  	/* Enable/Disable Interrupt */
  	Hal_KeyIntEnable = interruptEnable;

  	/* Register the callback fucntion */
  	pHalKeyProcessFunction = cback;

  	/* Determine if interrupt is enable or not */
  	if (Hal_KeyIntEnable)
  	{
		/* Interrupt configuration */

    	/* Do this only after the hal_key is configured - to work with sleep stuff */
    	if (HalKeyConfigured == TRUE)
    	{
      		meos_stop_timerEx(Hal_TaskID, HAL_KEY_EVENT);  /* Cancel polling if active */
    	}
  	}
  	else    /* Interrupts NOT enabled */
  	{
  		/* Cancel Key Interrupt */
		
    	meos_set_event(Hal_TaskID, HAL_KEY_EVENT);
  	}

  	/* Key now is configured */
  	HalKeyConfigured = TRUE;
}

uint8 HalKeyRead( void )
{
	return 0;
}

void HalKeyPoll( void )
{
  	uint8 keys = 0;
  
	keys |= HalKeyRead();
	
  	/* Invoke Callback if new keys were depressed */
  	if ( pHalKeyProcessFunction )
  	{
    	(pHalKeyProcessFunction)(keys, HAL_KEY_STATE_NORMAL);
  	}
}




