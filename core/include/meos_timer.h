/*
 *  Filename:      meos_timer.h
 *  Author:         Erick Huang<erickhuang1989@gmail.com>
 *  Revised:        2014-12-16
 *  Description:   Timer definition and manipulation functions.
 *
 *  Copyright (c) Erick Huang. All rights reserved.
 */

#ifndef MEOS_TIMER_H
#define MEOS_TIMER_H

#ifdef __cplusplus
extern "C" {
#endif


/* the unit is chosen such that the 320us tick equivalent can fit in 32 bits. */
#define MEOS_TIMERS_MAX_TIMEOUT   0x28f5c28e    /* in ms*/



extern void     meos_timer_init( void );
extern Status_t meos_start_timerEx( uint8 taskID, uint16 event_id, uint32 timeout_value );
extern Status_t meos_start_reload_timerEx( uint8 taskID, uint16 event_id, uint32 timeout_value );
extern Status_t meos_stop_timerEx( uint8 task_id, uint16 event_id );

extern void   meos_timer_update( uint32 updateTime );
extern uint32 meos_next_timeout( void );
extern uint32 meos_get_sys_clock( void );



#ifdef __cplusplus
}
#endif

#endif
