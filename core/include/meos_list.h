/*
 *  Filename:      meos_list.h
 *  Author:         Erick Huang<erickhuang1989@gmail.com>
 *  Revised:        2014-12-16
 *  Description:   
 *
 *  Copyright (c) Erick Huang. All rights reserved.
 */

#ifndef MEOS_LIST_H
#define MEOS_LIST_H

#ifdef __cplusplus
extern "C" {
#endif

/* Double-link list */
typedef struct _LIST {
	struct _LIST  *next;
	struct _LIST  *previous;
} LIST;

#define LIST_IS_EMPTY(element)   (((element)->next == (element)) ? TRUE : FALSE)

/*
 * List initialization
 */
#define list_init(list_head)	\
					do {\
						(list_head)->next = (list_head);\
						(list_head)->previous = (list_head);\
					 } while (0)

/*
 * Node initialization
 */
#define list_init_node(element)	\
					do {\
						(element)->next = NULL;\
						(element)->previous = NULL;\
					 } while (0)

/*
 * add element to list front.
 */
#define list_insert_front(head, element)    \
					do {\
						(element)->previous = (head);\
						(element)->next = (head)->next;\
						(head)->next = (element);\
						(head)->next->previous = (element);\
					 } while (0)


/*
 * add element to list tail.
 */
#define list_insert_tail(head, element)    \
					do {\
						(element)->previous = (head)->previous;\
						(element)->next = (head);\
						(head)->previous->next = (element);\
						(head)->previous = (element);\
					 } while (0)


/*
 * delete element from list
 */					
#define list_delete(element)          \
				do {\
					(element)->previous->next = (element)->next;\
					(element)->next->previous = (element)->previous;\
				} while (0)






#ifdef __cplusplus
}
#endif

#endif

