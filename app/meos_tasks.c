/*
 *  Filename:      meos_tasks.c
 *  Author:         Erick Huang<erickhuang1989@gmail.com>
 *  Revised:        2014-12-16
 *  Description:   This file contains function that allows user setup tasks.
 *
 *  Copyright (c) Erick Huang. All rights reserved.
 */
#include "meos_api.h"
#include "hal_drivers.h"
#include "meos_protocol.h"
#include "app.h"

// The order in this table must be identical to the task initialization calls below in meos_init_tasks.
const pTaskEventHandlerFn tasksArr[] =
{
	Hal_ProcessEvent, 	// task 0
	Prot_ProcessEvent,  // task 1
	App_ProcessEvent,	// task 2
};

#define TASKS_CNT     ARRAY_SIZE(tasksArr)

const uint8 tasksCnt = TASKS_CNT;
uint16 tasksEvents[TASKS_CNT] = {0};

/*********************************************************************
 * @fn         meos_init_tasks
 *
 * @brief     This function invokes the initialization function for each task.
 *
 * @param   void
 *
 * @return   none
 */
void meos_init_tasks( void )
{
  	uint8 taskID = 0;

  	/* Hal Task */
  	Hal_Init( taskID++ );

	/* Protocol Task */
  	Prot_Init( taskID++ );

  	/* Application */
  	App_Init( taskID );
}

