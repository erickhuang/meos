/*
 *  Filename:      app.h
 *  Author:         Erick Huang<erickhuang1989@gmail.com>
 *  Revised:        2014-12-23
 *  Description:   This file contains the sample application definitions and prototypes.
 *
 *  Copyright (c) Erick Huang. All rights reserved.
 */

#ifndef APP_H
#define APP_H

#ifdef __cplusplus
extern "C" {
#endif

typedef union
{
  	/* Request messages */
  	//ReadReq_t readReq;         // Read Request
  	//WriteReq_t writeReq;       // Write Request
  
  	/* Response messages */
  	//ErrorRsp_t errorRsp;        // Error Response
  	//ReadRsp_t readRsp;         // Read Response
} UserMsg_t;

typedef struct
{
  	meos_msg_data_hdr_t hdr; 
  	uint8     method;     // type of message
  	UserMsg_t msg;        // message contents
} UserMsgPacket_t;


//App Task Events
#define APP_START_DEVICE_EVT                  0x0001
#define APP_PERIODIC_EVT                      0x0002


// Delay between power-up and starting advertising (in ms)
#define APP_STARTDELAY                    300
// How often to perform periodic event (in ms)
#define APP_PERIODIC_EVT_PERIOD           1000

/*
 * Task Initialization for the user Application
 */
extern void App_Init( uint8 task_id );

/*
 * Task Event Processor for the user Application
 */
extern uint16 App_ProcessEvent( uint8 task_id, uint16 events );





#ifdef __cplusplus
}
#endif

#endif
