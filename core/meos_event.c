/*
 *  Filename:      meos_event.c
 *  Author:         Erick Huang<erickhuang1989@gmail.com>
 *  Revised:        2014-12-16
 *  Description:   
 *
 *  Copyright (c) Erick Huang. All rights reserved.
 */
#include "meos_api.h"

/*********************************************************************
 * @fn      meos_set_event
 *
 * @brief
 *
 *    This function is called to set the event flags for a task. The
 *    event passed in is OR'd into the task's event variable.
 *
 * @param   uint8 task_id - receiving tasks ID
 * @param   uint8 event_flag - what event to set
 *
 * @return  SUCCESS, INVALID_TASK
 */
Status_t meos_set_event( uint8 task_id, uint16 event_flag )
{
	if ( task_id < tasksCnt )
	{
    	halIntState_t   intState;
    	HAL_ENTER_CRITICAL_SECTION(intState);    // Hold off interrupts
    	tasksEvents[task_id] |= event_flag;      // Stuff the event bit(s)
    	HAL_EXIT_CRITICAL_SECTION(intState);     // Release interrupts
    	return SUCCESS;
  	}
	else
  	{
    	return INVALID_TASK;
  	}
}

/*********************************************************************
 * @fn      meos_clear_event
 *
 * @brief
 *
 *    This function is called to clear the event flags for a task. The
 *    event passed in is masked out of the task's event variable.
 *
 * @param   uint8 task_id - receiving tasks ID
 * @param   uint8 event_flag - what event to clear
 *
 * @return  SUCCESS, INVALID_TASK
 */
Status_t meos_clear_event( uint8 task_id, uint16 event_flag )
{
  	if ( task_id < tasksCnt )
  	{
		halIntState_t   intState;
	    HAL_ENTER_CRITICAL_SECTION(intState);    // Hold off interrupts
	    tasksEvents[task_id] &= ~(event_flag);   // Clear the event bit(s)
	    HAL_EXIT_CRITICAL_SECTION(intState);     // Release interrupts
	    return SUCCESS;
	}
	else
	{
		return INVALID_TASK;
	}
}

