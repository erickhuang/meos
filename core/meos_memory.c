/*
 *  Filename:      meos_memory.c
 *  Author:         Erick Huang<erickhuang1989@gmail.com>
 *  Revised:        2014-12-16
 *  Description:   This module defines the MEOS memory control functions. 
 *
 *  Copyright (c) Erick Huang. All rights reserved.
 */
#include "meos_api.h"


