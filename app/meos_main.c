/*
 *  Filename:      meos_main.c
 *  Author:         Erick Huang<erickhuang1989@gmail.com>
 *  Revised:        2014-12-16
 *  Description:   This file contains the main function.
 *
 *  Copyright (c) Erick Huang. All rights reserved.
 */
#include "meos_api.h"
#include "hal_board.h"

int main(void)
{
	// Initialize board I/O
	OnBoard_Init( OB_COLD );
	 
	/* Initialze the HAL driver */
	Hal_DriverInit();
	 
	/* Initialize the operating system */
	meos_init_system();
	 
	/* Enable interrupts */
	HAL_ENABLE_INTERRUPTS();
	 
	// Final board initialization
	OnBoard_Init( OB_READY );
	 
	#ifdef POWER_SAVING
		meos_pwrmgr_device( PWRMGR_BATTERY );
	#endif
	 
	/* Start MEOS */
	meos_start_system(); // No Return from here
	 
	return 0;
}

