/*
 *  Filename:      hal_sleep.h
 *  Author:         Erick Huang<erickhuang1989@gmail.com>
 *  Revised:        2014-12-16
 *  Description:   
 *
 *  Copyright (c) Erick Huang. All rights reserved.
 */

#ifndef HAL_SLEEP_H
#define HAL_SLEEP_H

#ifdef __cplusplus
extern "C" {
#endif


extern void hal_sleep( uint32 meos_timeout );



#ifdef __cplusplus
}
#endif

#endif
