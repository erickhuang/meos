/*
 *  Filename:      meos_cpu.h
 *  Author:         Erick Huang<erickhuang1989@gmail.com>
 *  Revised:        2014-12-16
 *  Description:   
 *
 *  Copyright (c) Erick Huang. All rights reserved.
 */

#ifndef MEOS_CPU_H
#define MEOS_CPU_H

#ifdef __cplusplus
extern "C" {
#endif

#include "meos_types.h"


/* Interrupt Macros*/
#define HAL_ENABLE_INTERRUPTS()
#define HAL_DISABLE_INTERRUPTS()
#define HAL_INTERRUPTS_ARE_ENABLED()

typedef uint8 halIntState_t;
#define HAL_ENTER_CRITICAL_SECTION(x)
#define HAL_EXIT_CRITICAL_SECTION(x)


/* disable interrupts, set watchdog timer, wait for reset */
#define HAL_SYSTEM_RESET()




#ifdef __cplusplus
}
#endif

#endif