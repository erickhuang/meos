/*
 *  Filename:      meos_types.h
 *  Author:         Erick Huang<erickhuang1989@gmail.com>
 *  Revised:        2014-12-16
 *  Description:   
 *
 *  Copyright (c) Erick Huang. All rights reserved.
 */

#ifndef MEOS_TYPES_H
#define MEOS_TYPES_H

#ifdef __cplusplus
extern "C" {
#endif

/* Data Types Defines */
typedef signed char   int8;      /* Signed 8 bit integer */
typedef unsigned char uint8;     /* Unsigned 8 bit integer */

typedef signed short    int16;   /* Signed 16 bit integer */
typedef unsigned short  uint16;  /* Unsigned 16 bit integer */

typedef signed long    int32;    /* Signed 32 bit integer */
typedef unsigned long  uint32;   /* Unsigned 32 bit integer */

typedef unsigned char  bool_t;     /* Boolean data type */


/* Standard Defines */
#ifndef TRUE
#define TRUE  1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef NULL
#define NULL  0
#endif


#if ( UINT_MAX == 65535 ) /* 8-bit and 16-bit devices */
	#define meos_offsetof(type, member) ((uint16)&(((type *)0)->member))
#else /* 32-bit devices */
	#define meos_offsetof(type, member) ((uint32)&(((type *)0)->member))
#endif


#ifdef __cplusplus
}
#endif

#endif